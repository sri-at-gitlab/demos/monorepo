# Monorepo

This is an example of pipelines for monorepo projects.

## Structure

This `monorepo` has two `microservices`:
- 🍎 `Microservice_Apple`
- 🍌 `Microservice_Banana`

## Pipeline definitions

- `.gitlab-ci.yml` root definition
- `common-rules.gitlab-ci.yml` shared rules
- `Microservice_Apple/apple.gitlab-ci.yml` apple pipeline
- `Microservice_Banana/banana.gitlab-ci.yml` banana pipeline

## Root `local` includes

```yaml
include:
    - local: common-rules.gitlab-ci.yml
    - local: Microservice_Apple/apple.gitlab-ci.yml
    - local: Microservice_Banana/banana.gitlab-ci.yml
```

## Rule definitions

- If Apple Changes
- If Apple Changes on Master
- If Banana Changes
- If Banana Changes on Master

### Changes based rules

```yaml
changes:
    - Microservice_Apple/*
when: on_success
```

### Branch based rules

```yaml
if: $CI_COMMIT_BRANCH == "master"
when: on_success
```

## Apply rules to jobs

```yaml
🍎 Build:
    stage: Build
    image: python:3
    script: echo "🍎"
    extends: .Rule_Run_If_Apple_Changes
```

## Test

- Making changes in `./Microservice_Apple` should only trigger the Apple pipeline
- Similar scheme for changes in `./Microservice_Banana`